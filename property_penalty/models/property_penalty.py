# -*- coding: utf-8 -*-
##############################################################################
#
#    OpenERP, Open Source Management Solution
#    Copyright (C) 2011-Today Serpent Consulting Services PVT LTD.
#    (<http://www.serpentcs.com>)
#
#    This program is free software: you can redistribute it and/or modify
#    it under the terms of the GNU Affero General Public License as
#    published by the Free Software Foundation, either version 3 of the
#    License, or (at your option) any later version.
#
#    This program is distributed in the hope that it will be useful,
#    but WITHOUT ANY WARRANTY; without even the implied warranty of
#    MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
#    GNU Affero General Public License for more details.
#
#
#    You should have received a copy of the GNU Affero General Public License
#    along with this program.  If not, see <http://www.gnu.org/licenses/>.
############################################################################

from openerp.osv import osv, fields
from openerp.tools.translate import _
from dateutil.relativedelta import relativedelta
from datetime import datetime
from openerp.tools import DEFAULT_SERVER_DATE_FORMAT


class account_analytic_account(osv.osv):
    _inherit = "account.analytic.account"

    _columns = {
        'penalty':fields.float('Penalty (%)'),
        'penalty_day':fields.integer('Penalty Count After Days'),
    }


class tenancy_rent_schedule(osv.Model):
    _inherit = "tenancy.rent.schedule"
    _rec_name = "tenancy_id"
    _order = 'start_date'


    def create_move(self, cr, uid, ids, context=None):
        """
            This button Method is used to create account move
            with penalty calculation functionality.
        @param self: The object pointer
        @param cr: the current row, from the database cursor,
        @param uid: the current user’s ID for security checks,
        @param ids: List of IDs
        @param context: A standard dictionary for contextual values
        """
        context = dict(context or {})
        can_close = False
        move_obj = self.pool.get('account.move')
        move_line_objt = self.pool.get('account.move.line')
        created_move_ids = []
        journal_ids = self.pool.get('account.journal').search(cr, uid, [('type', '=', 'sale')])
        exp_acc_id = self.pool.get('account.account').search(cr, uid, [('type', '=', 'other'),'|',('code', '=', '220000'),('name', '=', 'Expenses')])
        for line in self.browse(cr, uid, ids, context=context):
            line_amount = line.tenancy_id.rent
            full_amount = line.tenancy_id.rent
            depreciation_date = datetime.now()
            period_ids = self.pool.get('account.period').find(cr, uid, depreciation_date, context=context)
            company_currency = line.tenancy_id.company_id.currency_id.id
            current_currency = line.tenancy_id.currency_id.id
            sign = -1
            move_vals = {
                'name': line.tenancy_id.name,
                'date': depreciation_date,
                'ref': line.tenancy_id.code,
                'period_id': period_ids and period_ids[0] or False,
                'journal_id': journal_ids and journal_ids[0],
                }
            move_id = move_obj.create(cr, uid, move_vals, context=context)
            if not line.tenancy_id.property_id.income_acc.id:
                raise Warning(_('Please Configure Income Account from Property'))
            today_date = datetime.today().date()
            ten_date = datetime.strptime(line.start_date, DEFAULT_SERVER_DATE_FORMAT).date()
            if line.tenancy_id.penalty_day != 0:
                ten_date = ten_date + relativedelta(days = int(line.tenancy_id.penalty_day))
            if ten_date < today_date :
                remaining_days = (today_date - ten_date)
                if remaining_days.days:
                    line_amount_day = (line.tenancy_id.rent * line.tenancy_id.penalty)/100
                    line_amount = line_amount_day * float(remaining_days.days)
                    full_amount += line_amount
            move_line_objt.create(cr, uid, {
                                    'name': line.tenancy_id.name,
                                    'ref': line.tenancy_id.code,
                                    'move_id': move_id,
                                    'account_id': line.tenancy_id.property_id.income_acc.id or False,
                                    'debit': 0.0,
                                    'credit': full_amount,
                                    'period_id': period_ids and period_ids[0] or False,
                                    'journal_id': journal_ids and journal_ids[0],
                                    'partner_id': line.tenancy_id.tenant_id.parent_id.id or False,
                                    'currency_id': company_currency != current_currency and  current_currency or False,
                                    'amount_currency': company_currency != current_currency and - sign * line.tenancy_id.rent or 0.0,
                                    'date': depreciation_date,
                                    })
            if ten_date < today_date :
                move_line_objt.create(cr, uid, {
                                        'name': 'Penalty - ' + str(line.tenancy_id.name),
                                        'ref': line.tenancy_id.code,
                                        'move_id': move_id,
                                        'account_id': exp_acc_id and exp_acc_id[0] or line.tenancy_id.tenant_id.property_account_receivable.id,
                                        'credit': 0.0,
                                        'debit': line_amount,
                                        'period_id': period_ids and period_ids[0] or False,
                                        'journal_id': journal_ids and journal_ids[0],
                                        'partner_id': line.tenancy_id.tenant_id.parent_id.id or False,
                                        'currency_id': company_currency != current_currency and  current_currency,
                                        'amount_currency': company_currency != current_currency and sign * line.tenancy_id.rent or 0.0,
                                        'analytic_account_id': line.tenancy_id.id,
                                        'date': depreciation_date,
                                        'asset_id': line.tenancy_id.property_id.id or False,
                                        })
            move_line_objt.create(cr, uid, {
                                    'name': 'Rent - ' + str(line.tenancy_id.name),
                                    'ref': line.tenancy_id.code,
                                    'move_id': move_id,
                                    'account_id': line.tenancy_id.tenant_id.property_account_receivable.id,
                                    'credit': 0.0,
                                    'debit': line.tenancy_id.rent,
                                    'period_id': period_ids and period_ids[0] or False,
                                    'journal_id': journal_ids and journal_ids[0],
                                    'partner_id': line.tenancy_id.tenant_id.parent_id.id or False,
                                    'currency_id': company_currency != current_currency and  current_currency,
                                    'amount_currency': company_currency != current_currency and sign * line.tenancy_id.rent or 0.0,
                                    'analytic_account_id': line.tenancy_id.id,
                                    'date': depreciation_date,
                                    'asset_id': line.tenancy_id.property_id.id or False,
                                    })
            self.write(cr, uid, [line.id], {'move_id': move_id}, context=context)
            created_move_ids.append(move_id)
            move_obj.write(cr, uid, [move_id], {'state':'posted'}, context=context)
        return created_move_ids

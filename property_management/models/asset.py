# -*- coding: utf-8 -*-
##############################################################################
#
# 	OpenERP, Open Source Management Solution
# 	Copyright (C) 2011-Today Serpent Consulting Services PVT LTD (<http://www.serpentcs.com>)
#
# 	This program is free software: you can redistribute it and/or modify
# 	it under the terms of the GNU Affero General Public License as
# 	published by the Free Software Foundation, either version 3 of the
# 	License, or (at your option) any later version.
#
# 	This program is distributed in the hope that it will be useful,
# 	but WITHOUT ANY WARRANTY; without even the implied warranty of
# 	MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
# 	GNU Affero General Public License for more details.
#
# 	You should have received a copy of the GNU Affero General Public License
# 	along with this program.  If not, see <http://www.gnu.org/licenses/>.
#
############################################################################

import time
import webbrowser
from datetime import datetime, date
from openerp.osv import osv, fields
from openerp.tools.translate import _
from openerp.exceptions import Warning
import openerp.addons.decimal_precision as dp
from dateutil.relativedelta import relativedelta
from openerp.tools import DEFAULT_SERVER_DATE_FORMAT


class crossovered_budget_lines(osv.osv):

	_inherit = "crossovered.budget.lines"

	_columns = {
		'asset_id':fields.many2one('account.asset.asset','Property'),
	}


class account_asset_asset(osv.osv):
	_inherit = 'account.asset.asset'
	_description = 'Asset'


	def _has_image(self, cr, uid, ids, field_name, arg, context=None):
		"""
			This method is used to set Property image.
		@param self: The object pointer
		@param cr: the current row, from the database cursor,
		@param uid: the current user’s ID for security checks,
		@param ids: List of IDs
		@param name: Names of fields.
		@param arg: User defined arguments
		@param context: A standard dictionary for contextual values
		@return: Dictionary of values.
		"""
		return dict((p.id, bool(p.image)) for p in self.browse(cr, uid, ids, context=context))

	def occupancy_calculation(self, cr, uid, ids, field_name, arg, context=None):
		"""
			This Method is used to calculate occupancy rate.
		@param self: The object pointer
		@param cr: the current row, from the database cursor,
		@param uid: the current user’s ID for security checks,
		@param ids: List of IDs
		@param name: Names of fields.
		@param arg: User defined arguments
		@param context: A standard dictionary for contextual values
		@return: Dictionary of values.
		"""
		rec = {}
		occ = 0
		diff = 0
		for prop_res in self.browse(cr, uid, ids, context=context):
			cur_datetime = datetime.now().strftime(DEFAULT_SERVER_DATE_FORMAT)
			purchase_diff2 = datetime.strptime(cur_datetime, DEFAULT_SERVER_DATE_FORMAT) - datetime.strptime(prop_res.purchase_date, DEFAULT_SERVER_DATE_FORMAT)
			purchase_diff = purchase_diff2.days
			if prop_res.tenancy_property_ids and prop_res.tenancy_property_ids.ids:
				for tenancy_rec in prop_res.tenancy_property_ids:
					if tenancy_rec.date and tenancy_rec.date_start:
						ten_diff = datetime.strptime(tenancy_rec.date, DEFAULT_SERVER_DATE_FORMAT) - datetime.strptime(tenancy_rec.date_start, DEFAULT_SERVER_DATE_FORMAT)
						diff += ten_diff.days
				if (purchase_diff != 0 and diff != 0):
					occ = diff / purchase_diff
			rec[prop_res.id] = occ
		return rec

	def sales_rate_calculation(self, cr, uid, ids, field_name, arg, context=None):
		"""
			This Method is used to calculate total sales rates.
		@param self: The object pointer
		@param cr: the current row, from the database cursor,
		@param uid: the current user’s ID for security checks,
		@param ids: List of IDs
		@param name: Names of fields.
		@param arg: User defined arguments
		@param context: A standard dictionary for contextual values
		@return: Dictionary of values.
		"""
		rec = {}
		sales_rate = 0
		counter = 0
		sr = 0
		for prop_res in self.browse(cr, uid, ids, context=context):
			if prop_res.property_phase_ids and prop_res.property_phase_ids.ids:
				for phase_rec in prop_res.property_phase_ids:
					counter = counter + 1
					sales_rate += phase_rec.lease_price
				if (sales_rate != 0 and counter != 0):
					sr = sales_rate / counter
			rec[prop_res.id] = sr
		return rec

	def roi_calculation(self, cr, uid, ids, field_name, arg, context=None):
		"""
			This Method is used to Calculate ROI(Return On Investment).
		@param self: The object pointer
		@param cr: the current row, from the database cursor,
		@param uid: the current user’s ID for security checks,
		@param ids: List of IDs
		@param name: Names of fields.
		@param arg: User defined arguments
		@param context: A standard dictionary for contextual values
		@return: Dictionary of values.
		"""
		rec = {}
		cost_of_investment = 0
		gain_from_investment = 0
		roi = 0
		for prop_res in self.browse(cr, uid, ids, context=context):
			if prop_res.maintenance_ids and prop_res.maintenance_ids.ids:
				for maintenance_rec in prop_res.maintenance_ids:
					cost_of_investment += maintenance_rec.cost
				for tenancy_rec in prop_res.tenancy_property_ids:
					gain_from_investment += tenancy_rec.rent
				if (cost_of_investment != 0 and gain_from_investment != 0):
					roi = (gain_from_investment - cost_of_investment) / cost_of_investment
			rec[prop_res.id] = roi
		return rec

	def ten_year_roi_calculation(self, cr, uid, ids, field_name, arg, context=None):
		"""
			This Method is used to Calculate ten years ROI(Return On Investment).
		@param self: The object pointer
		@param cr: the current row, from the database cursor,
		@param uid: the current user’s ID for security checks,
		@param ids: List of IDs
		@param name: Names of fields.
		@param arg: User defined arguments
		@param context: A standard dictionary for contextual values
		@return: Dictionary of values.
		"""
		rec = {}
		for prop_res in self.browse(cr, uid, ids, context=context):
			rec[prop_res.id] = 10 * (prop_res.roi)
		return rec

	def calc_return_period(self, cr, uid, ids, field_name, arg, context=None):
		"""
			This Method is used to Calculate Return Period.
		@param self: The object pointer
		@param cr: the current row, from the database cursor,
		@param uid: the current user’s ID for security checks,
		@param ids: List of IDs
		@param name: Names of fields.
		@param arg: User defined arguments
		@param context: A standard dictionary for contextual values
		@return: Dictionary of values.
		"""
		rec = {}
		rtn_prd = 0
		for prop_res in self.browse(cr, uid, ids, context=context):
			if (prop_res.purchase_price != 0 and prop_res.ground_rent != 0):
				rtn_prd = prop_res.purchase_price / prop_res.ground_rent
			rec[prop_res.id] = rtn_prd
		return rec

	def operation_cost(self, cr, uid, ids, field_name, arg, context=None):
		"""
			This Method is used to Calculate Operation Cost.
		 @param self: The object pointer
		@param cr: the current row, from the database cursor,
		@param uid: the current user’s ID for security checks,
		@param ids: List of IDs
		@param name: Names of fields.
		@param arg: User defined arguments
		@param context: A standard dictionary for contextual values
		@return: Dictionary of values.
		"""
		rec = {}
		operational_cost = 0
		gain_from_investment = 0
		opr_cst = 0
		for res in self.browse(cr, uid, ids, context=context):
			for gain in res.tenancy_property_ids:
				gain_from_investment += gain.rent
			for data in res.property_phase_ids:
				operational_cost += ((data.operational_budget * data.lease_price) / 100)
			if (gain_from_investment != 0 and operational_cost != 0):
				opr_cst = operational_cost / gain_from_investment
			rec[res.id] = opr_cst
		return rec

	def cal_simulation(self, cr, uid, ids, field_name, arg, context=None):
		"""
			This Method is used to calculate simulation
			which is used in Financial Performance Report.
		@param self: The object pointer
		@param cr: the current row, from the database cursor,
		@param uid: the current user’s ID for security checks,
		@param ids: List of IDs
		@param name: Names of fields.
		@param arg: User defined arguments
		@param context: A standard dictionary for contextual values
		@return: Dictionary of values.
		"""
		amt = 0.0
		for property_data in self.browse(cr, uid, ids, context=context):
			tncy_lst = [tncy.id for tncy in property_data.tenancy_property_ids]
			sort_tncy = list(set(tncy_lst))
			for tncy_data in self.pool.get('account.analytic.account').browse(cr, uid, sort_tncy, context=context):
				if tncy_data.rent_schedule_ids and tncy_data.rent_schedule_ids.ids:
					for prty_tncy_data in tncy_data.rent_schedule_ids:
						amt += prty_tncy_data.amount
		return {ids[0]:amt}

	def cal_revenue(self, cr, uid, ids, field_name, arg, context=None):
		"""
			This Method is used to calculate revenue
			which is used in Financial Performance Report.
		@param self: The object pointer
		@param cr: the current row, from the database cursor,
		@param uid: the current user’s ID for security checks,
		@param ids: List of IDs
		@param name: Names of fields.
		@param arg: User defined arguments
		@param context: A standard dictionary for contextual values
		@return: Dictionary of values.
		"""
		amt = 0.0
		for property_data in self.browse(cr, uid, ids, context=context):
			tncy_lst = [tncy.id for tncy in property_data.tenancy_property_ids]
			sort_tncy = list(set(tncy_lst))
			for tncy_data in self.pool.get('account.analytic.account').browse(cr, uid, sort_tncy, context=context):
				for prty_tncy_data in tncy_data.rent_schedule_ids:
					if prty_tncy_data.move_check == True:
						amt += prty_tncy_data.amount
		return {ids[0]:amt}

	def cal_total_price(self, cr, uid, ids, field_name, arg, context=None):
		"""
			This Method is used to Calculate Total Price.
		@param self: The object pointer
		@param cr: the current row, from the database cursor,
		@param uid: the current user’s ID for security checks,
		@param ids: List of IDs
		@param name: Names of fields.
		@param arg: User defined arguments
		@param context: A standard dictionary for contextual values
		@return: Dictionary of values.
		"""
		rec = {}
		for property_data in self.browse(cr, uid, ids, context=context):
			rec[property_data.id] = property_data.gfa_feet * property_data.unit_price
		return rec

	def _get_rent(self, cr, uid, ids, context=None):
		"""
			Returns keys of dictionary from account analytic account
			when calculate ROI and Operational costs.
		@param self: The object pointer
		@param cr: the current row, from the database cursor,
		@param uid: the current user’s ID for security checks,
		@param ids: List of IDs
		@param context: A standard dictionary for contextual values
		"""
		result = {}
		for line in self.pool.get('account.analytic.account').browse(cr, uid, ids, context=context):
			result[line.property_id.id] = True
		return result.keys()

	def _get_lease(self, cr, uid, ids, context=None):
		"""
			Returns keys of dictionary from property phase
			when calculate Operational costs.
		@param self: The object pointer
		@param cr: the current row, from the database cursor,
		@param uid: the current user’s ID for security checks,
		@param ids: List of IDs
		@param context: A standard dictionary for contextual values
		"""
		result = {}
		for line in self.pool.get('property.phase').browse(cr, uid, ids, context=context):
			result[line.phase_id.id] = True
		return result.keys()

	def _get_cost(self, cr, uid, ids, context=None):
		"""
			Returns keys of dictionary from property maintenance
			when calculate ROI.
		@param self: The object pointer
		@param cr: the current row, from the database cursor,
		@param uid: the current user’s ID for security checks,
		@param ids: List of IDs
		@param context: A standard dictionary for contextual values
		"""
		result = {}
		for line in self.pool.get('property.maintenance').browse(cr, uid, ids, context=context):
			result[line.property_id.id] = True
		return result.keys()

	def get_date(self, cr, uid, ids, context=None):
		"""
			Returns keys of dictionary from account_analytic_account
			when calculate Occupancy rates. 
		@param self: The object pointer
		@param cr: the current row, from the database cursor,
		@param uid: the current user’s ID for security checks,
		@param ids: List of IDs
		@param context: A standard dictionary for contextual values
		"""
		result = {}
		for line in self.pool.get('account.analytic.account').browse(cr, uid, ids, context=context):
			result[line.property_id.id] = True
		return result.keys()

	def get_amt_test(self, cr, uid, ids, context=None):
		"""
			Returns keys of dictionary from account analytic account
			when calculate simulation amount.
		@param self: The object pointer
		@param cr: the current row, from the database cursor,
		@param uid: the current user’s ID for security checks,
		@param ids: List of IDs
		@param context: A standard dictionary for contextual values
		"""
		result = {}
		for line in self.pool.get('account.analytic.account').browse(cr, uid, ids, context=context):
			result[line.property_id.id] = True
		return result.keys()

	def _get_check(self, cr, uid, ids, context=None):
		"""
			Returns keys of dictionary from tenancy rent schedule
			when calculate simulation amounts.
		@param self: The object pointer
		@param cr: the current row, from the database cursor,
		@param uid: the current user’s ID for security checks,
		@param ids: List of IDs
		@param context: A standard dictionary for contextual values
		"""
		result = {}
		for line in self.pool.get('tenancy.rent.schedule').browse(cr, uid, ids, context=context):
			result[line.property_id.id] = True
		return result.keys()

	def _get_check_test(self, cr, uid, ids, context=None):
		"""
			Returns keys of dictionary from tenancy rent schedule
			when calculate Revenue. 
		@param self: The object pointer
		@param cr: the current row, from the database cursor,
		@param uid: the current user’s ID for security checks,
		@param ids: List of IDs
		@param context: A standard dictionary for contextual values
		"""
		result = {}
		for line in self.pool.get('tenancy.rent.schedule').browse(cr, uid, ids, context=context):
			result[line.property_id.id] = True
		return result.keys()

	def _amount_residual_inherit(self, cr, uid, ids, name, args, context=None):
		"""
		@param self: The object pointer
		@param cr: the current row, from the database cursor,
		@param uid: the current user’s ID for security checks,
		@param ids: List of IDs
		@param name: Names of fields.
		@param arg: User defined arguments
		@param context: A standard dictionary for contextual values
		@return: Dictionary of values.
		"""
		cr.execute("""SELECT
				l.asset_id as id, SUM(abs(l.debit-l.credit)) AS amount
			FROM
				account_move_line l
			WHERE
				l.asset_id IN %s GROUP BY l.asset_id """, (tuple(ids),))
		res = dict(cr.fetchall())
		for asset in self.browse(cr, uid, ids, context=context):
			current_currency = asset.currency_id and asset.currency_id.id or False
			amount = self.pool['res.currency'].compute(cr, uid, asset.company_id.currency_id.id, current_currency, res.get(asset.id, 0.0), context=context)
			res[asset.id] = asset.purchase_value - amount - asset.salvage_value
			if res[asset.id] < 0.0:
				res[asset.id] = asset.purchase_value
		for id in ids:
			res.setdefault(id, 0.0)
		return res

	_columns = {
		'type_id':fields.many2one('property.type', 'Property Type', help='Property Type'),
		'furnished':fields.selection([('none', 'None'), ('semi_furnished', 'Semi Furnished'),
									  ('full_furnished', 'Full Furnished')], 'Furnishing', help='Furnishing'),
		'state': fields.selection([('new_draft', 'Booking Open'), ('draft', 'Available'), ('book', 'Booked'), ('normal', 'On Lease'), ('close', 'Sale'), ('sold', 'Sold'), ('cancel', 'Cancel')], 'State', required=True),
		'contact_id':fields.many2one('tenant.partner', 'Contact Name', domain="[('tenant', '=', True)]"),
		'street': fields.char('Street'),
		'street2': fields.char('Street2'),
		'zip': fields.char('Zip', size=24, change_default=True),
		'township': fields.char('Township'),
		'city': fields.char('City'),
		'irr':fields.float('IRR'),
		'state_id': fields.many2one("res.country.state", 'State', ondelete='restrict'),
		'country_id': fields.many2one('res.country', 'Country', ondelete='restrict'),
		'room_ids':fields.one2many('property.room', 'property_id', 'Rooms'),
		'maintenance_ids':fields.one2many('property.maintenance', 'property_id', 'Maintenance'),
		'utility_ids':fields.one2many('property.utility', 'property_id', 'Utilities'),
		'safety_certificate_ids':fields.one2many('property.safety.certificate', 'property_id', 'Safety Certificate'),
		'note':fields.text('Notes'),
		'purchase_price':fields.float('Purchase Price', help='Purchase Price of the Property'),
		'multiple_owners':fields.boolean('Multiple Owners', help="Check this box if there is multiple Owner of the Property."),
		'total_owners':fields.integer('Number of Owners'),
		'purchase_cost_ids':fields.one2many('cost.cost', 'purchase_property_id', 'Costs'),
		'sale_date':fields.date('Sale Date', help='Sale Date of the Property'),
		'sale_price':fields.float('Sale Price', help='Sale Price of the Property'),
		'sale_cost_ids':fields.one2many('sale.cost', 'sale_property_id', 'Costs'),
		'rent_type_id':fields.many2one('rent.type', 'Rent Type', help='Type of the Rent'),
		'ground_rent':fields.float('Ground Rent'),
		'value_residual': fields.function(_amount_residual_inherit, method=True, digits_compute=dp.get_precision('Account'), string='Residual Value'),
		'property_insurance_ids':fields.one2many('property.insurance', 'property_insurance_id', 'Insurance'),
		'tenancy_property_ids':fields.one2many('account.analytic.account', 'property_id', 'Tenancy Property'),
		'contract_attachment_ids' : fields.one2many('property.attachment', 'property_id', 'Document'),
		'analytic_acc_id':fields.many2one('account.analytic.account', 'Anlytic Account'),
		'color': fields.integer('Color'),
		'gfa_feet':fields.float('GFA(Sqft)', help='Gross Floor Area in Square Feet'),
		'gfa_meter':fields.float('GFA(m)', help='Gross Floor Area in Meter'),
		'property_manager':fields.many2one('res.partner', 'Property Manager', help="Manager of Property"),
		'property_photo_ids':fields.one2many('property.photo', 'photo_id', 'Photos'),
		'image': fields.binary("Image"),
		'has_image': fields.function(_has_image, type="boolean"),
		'ownership':fields.selection([('freehold', 'Freehold'), ('freehold1', 'Freehold'), ('leasehold', 'Leasehold'), ('bot', 'BOT')], 'Ownership'),
		'green_vision_title':fields.char('Green Vision title'),
		'third_party_title':fields.char('Third Party title'),
		'conditions':fields.char('Conditions'),
		'start_date':fields.date('Start Date'),
		'end_date':fields.date('End Date'),
		'simulation_name':fields.char('Simulation name'),
		'simulation_date':fields.date('Simulation date'),
		'construction_cost':fields.char('Construction Cost'),
		'property_phase_ids':fields.one2many('property.phase', 'phase_id', 'Phase'),
		'financial_performance':fields.float('Financial performance(%)'),
		'roi':fields.function(roi_calculation, type="float", string="ROI", help="Return On Investment", store={
										'account.analytic.account': (_get_rent, ['rent'], 20),
										'property.maintenance': (_get_cost, ['cost'], 20),
										'account.asset.asset': (lambda self, cr, uid, ids, c={}: ids, ['tenancy_property_ids,', 'maintenance_ids'], 10)}),
		'ten_year_roi':fields.function(ten_year_roi_calculation, type="float", string="10year ROI", help="10year Return On Investment"),
		'return_period':fields.function(calc_return_period, type="float", string="Return Period(In Months)",store={
																	'account.asset.asset': (lambda self, cr, uid, ids, c={}: ids, ['purchase_price', 'ground_rent'], 10)}),
		'operational_costs':fields.function(operation_cost, type="float", string="Operational costs(%)", store={
										'account.analytic.account': (_get_rent, ['rent'], 20),
										'property.phase': (_get_lease, ['operational_budget','lease_price'], 20),
										'account.asset.asset': (lambda self, cr, uid, ids, c={}: ids, ['tenancy_property_ids,', 'property_phase_ids'], 10)}),
		'crossovered_budget_line_property': fields.one2many('crossovered.budget.lines', 'asset_id', 'Budget Lines'),
		'occupancy_rates':fields.function(occupancy_calculation, type="float", string="Occupancy rate", store={
										'account.analytic.account': (get_date, ['date', 'date_start'], 20),
										'account.asset.asset': (lambda self, cr, uid, ids, c={}: ids, ['tenancy_property_ids,', 'purchase_date'], 10)}),
		'sales_rates':fields.function(sales_rate_calculation, type="float", string="Sales Rate"),
		'simulation' :fields.function(cal_simulation, type='float', string='Total Amount', store={
										'account.analytic.account': (get_amt_test, ['rent_schedule_ids'], 10),
										'tenancy.rent.schedule': (_get_check, ['amount'], 10),
										'account.asset.asset': (lambda self, cr, uid, ids, c={}: ids, ['tenancy_property_ids'], 10)}),
		'revenue' :fields.function(cal_revenue, type='float', string='Revenue', store={
										'tenancy.rent.schedule': (_get_check_test, [], 10),
										'account.asset.asset': (lambda self, cr, uid, ids, c={}: ids, ['tenancy_property_ids'], 10)}),
		'unit_price':fields.float('Unit Price'),
		'total_price':fields.function(cal_total_price, type='float', string='Total Price'),
		'depreciation_line_ids': fields.one2many('account.asset.depreciation.line', 'asset_id', 'Depreciation Lines', readonly=True, states={'draft':[('readonly', False)]}),
		'income_acc' : fields.many2one('account.account', 'Income Account'),
		'facing':fields.selection([('north', 'North'), ('south', 'South'), ('east', 'East'), ('west', 'West')], 'Facing'),
		'age_of_property':fields.date('Age of Property'),
		'floor':fields.integer('Floor', help='Number Of Floors'),
		'doc_name' : fields.char('Filename'),
		'nearby_ids':fields.one2many('nearby.property', 'property_id', 'Nearest Property'),
		'bedroom':fields.selection([('1', '1'), ('2', '2'), ('3', '3'), ('4', '4'), ('5', '5+')], 'Bedrooms'),
		'bathroom':fields.selection([('1', '1'), ('2', '2'), ('3', '3'), ('4', '4'), ('5', '5+')], 'Bathrooms'),
		'recurring_rule_type': fields.selection([('monthly', 'Month(s)'),], 'Recurrency', help="Invoice automatically repeat at specified interval"),
		'no_of_towers':fields.integer('No Of Towers', help='Number Of Towers'),
		'no_of_property':fields.integer('Property Per Floors', help='Number Of Properties Per Floor'),
		'video_url':fields.char('Video URL', help="//www.youtube.com/embed/mwuPTI8AT7M?rel=0"),
		'current_tenant':fields.many2one('tenant.partner', 'Current Tenant'),
		'customer_id':fields.many2one('res.partner', 'Customer'),
		'payment_term':fields.many2one('account.payment.term', 'Payment Terms'),
		'pur_instl_chck':fields.boolean('Purchase Installment Check'),
		'sale_instl_chck':fields.boolean('Sale Installment Check'),
		'purpose':fields.selection([('for_lease', 'For Lease'), ('for_sale', 'For Sale')], 'Purpose'),
	}

	_defaults = {
		'state':'draft',
		'color': 4,
		'age_of_property': lambda *a: time.strftime(DEFAULT_SERVER_DATE_FORMAT),
		'recurring_rule_type':'monthly',
		'pur_instl_chck':False,
		'sale_instl_chck':False,
		'furnished':'none',
		'bedroom':'1',
		'bathroom':'1',
	}

	def _check_secondary_photo(self, cr, uid, ids, context=None):
		account_assets_obj= self.browse(cr,uid,ids,context=context)
		property_photo_true = []
		for one_photo_obj in account_assets_obj.property_photo_ids:
			one_property_photo_obj_true = one_photo_obj.secondary_photo
			property_photo_true.append(one_property_photo_obj_true)
		if property_photo_true.count(True) > 1:
			return False
		return True

	_constraints = [
        (_check_secondary_photo, 'Error!\nSecondary photo is filled if you are change photo please remove first one.', ['property_photo_ids']),
    ]

	def create(self, cr, uid, vals, context=None):
		"""
		This Method is used to overrides orm create method.
		@param self: The object pointer
		@param cr: the current row, from the database cursor,
		@param uid: the current user’s ID for security checks,
		@param vals: dictionary of fields value.
		@param context: A standard dictionary for contextual values
		"""
		# if vals.has_key('property_photo_ids'):
		# 	property_photos = vals['property_photo_ids']
		# 	property_photo_true = []
		# 	for one_property_photo in property_photos:
		# 		one_property_photo_id = one_property_photo[1]
		# 		property_photo_true.append(one_property_photo[2]['secondary_photo'])
		# 	if property_photo_true.count(True) > 1:
		# 		raise osv.except_osv(_("Warning!"), _("Please select only one secondary photo field."))

		if not vals:
			vals = {}
		vals['code'] = self.pool.get('ir.sequence').get(cr, uid, 'property', context=context)
		asset_id = super(account_asset_asset, self).create(cr, uid, vals, context=context)
		self.pool.get('account.analytic.account').create(cr, uid, {'name' : vals['name']}, context=context)
		return asset_id

	def write(self, cr, uid, ids, vals, context=None):
		"""
		This Method is used to overrides orm write method.
		@param self: The object pointer
		@param cr: the current row, from the database cursor,
		@param uid: the current user’s ID for security checks,
		@param ids: List of IDs
		@param vals: dictionary of fields value.
		@param context: A standard dictionary for contextual values
		"""
		# aaa= super(account_asset_asset, self).search(cr, uid, ids, context=context)
		# print "----------aaaa--------",self.browse(cr,uid,ids,context=context)
		# if vals.has_key('property_photo_ids'):
		# 	print "==================vals==============",vals['property_photo_ids']
		# 	property_photos = vals['property_photo_ids']
		# 	property_photo_id = []
		# 	for one_property_photo in property_photos:
		# 		one_property_photo_id = one_property_photo[1]
		# 		property_photo_id.append(one_property_photo_id)
		# 		print ".....................",one_property_photo[2]
        #
		# 	print "=============property_photo_id=======",property_photo_id
		# 	if property_photo_id:
		# 		photo_obj_ids = self.pool.get('property.photo').search(cr, uid,[('id', 'in', property_photo_id),('secondary_photo', '=', True)], context=context)
		# 		print "-----------photo_obj_ids--------",photo_obj_ids
		# 		if photo_obj_ids:
		# 			raise osv.except_osv(_("Warning!"), _("You have select multiple secondary photo field."))

		if vals.has_key('state') and vals['state'] == 'new_draft':
			vals.update({'color':0})
		if vals.has_key('state') and vals['state'] == 'draft':
			vals.update({'color':4})
		if vals.has_key('state') and vals['state'] == 'book':
			vals.update({'color':2})
		if vals.has_key('state') and vals['state'] == 'normal':
			vals.update({'color':7})
		if vals.has_key('state') and vals['state'] == 'close':
			vals.update({'color':9})
		if vals.has_key('state') and vals['state'] == 'sold':
			vals.update({'color':9})
		if vals.has_key('state') and vals['state'] == 'cancel':
			vals.update({'color':1})
		return super(account_asset_asset, self).write(cr, uid, ids, vals, context=context)

	def sqft_to_meter(self, cr, uid, ids, gfa_feet, context=None):
		"""
			when you change gfa_feet field value, this method will change
			gfa_meter field value accordingly.
		@param self: The object pointer
		@param cr: the current row, from the database cursor,
		@param uid: the current user’s ID for security checks,
		@param ids: List of IDs
		@param context: A standard dictionary for contextual values
		@return: Dictionary of values.
		"""
		if gfa_feet:
			return {'value' : {'gfa_meter':float(gfa_feet / 10.7639104)}}
		return {}

	def unit_price_calc(self, cr, uid, ids, unit_price, gfa_feet, context=None):
		"""
			when you change unit_price and gfa_feet fields value, this method will change
			total_price and purchase_value fields value accordingly.
		@param self: The object pointer
		@param cr: the current row, from the database cursor,
		@param uid: the current user’s ID for security checks,
		@param ids: List of IDs
		@param context: A standard dictionary for contextual values
		@return: Dictionary of values.
		"""
		if unit_price and gfa_feet:
			return {'value': {'total_price' : float(unit_price * gfa_feet), 'purchase_value' : float(unit_price * gfa_feet)}}
		if unit_price and not gfa_feet:
			raise osv.except_osv(('Error!'), ('Please Insert GFA(Sqft) Please'))
		return {}

	def parent_property_onchange(self, cr, uid, ids, parent_id, context=None):
		"""
			when you change parent_id field value, this method will change
			address fields values accordingly.
		@param self: The object pointer
		@param cr: the current row, from the database cursor,
		@param uid: the current user’s ID for security checks,
		@param ids: List of IDs
		@param context: A standard dictionary for contextual values
		@return: Dictionary of values.
		"""
		parent_data = self.browse(cr, uid, parent_id, context=context)
		return {
			'value' : {
				'street' : parent_data.street or '', 'street2' : parent_data.street2 or '',
				'township' : parent_data.township or '', 'city' : parent_data.city or '' , 'state_id' : parent_data.state_id and parent_data.state_id.id or False,
				'zip' : parent_data.zip or '', 'country_id' : parent_data.country_id and parent_data.country_id.id or False
			}
		}

	def edit_status(self, cr, uid, ids, context=None):
		"""
			This method is used to change property state
			to book.
		@param self: The object pointer
		@param cr: the current row, from the database cursor,
		@param uid: the current user’s ID for security checks,
		@param ids: List of IDs
		@param context: A standard dictionary
		"""
		status = {}
		for rec in self.browse(cr, uid, ids, context=context):
			if not rec.property_manager:
				raise osv.except_osv(('Warning!'), ('Please Insert Owner Name'))
			if rec.state == 'draft':
				status.update({'state': 'book'})
		return self.write(cr, uid, [rec.id], status, context=context)

	def edit_status_book(self, cr, uid, ids, context=None):
		"""
			This method will open a wizard.
		@param self: The object pointer
		@param cr: the current row, from the database cursor,
		@param uid: the current user’s ID for security checks,
		@param ids: List of IDs
		@param context: A standard dictionary
		"""
		for rec in self.browse(cr, uid, ids, context=context):
			context.update({'result3':rec.id})
		return {
			'name': ('wizard'),
			'res_model': 'book.available',
			'type': 'ir.actions.act_window',
			'view_id': False,
			'view_mode': 'form',
			'view_type': 'form',
			'target':'new',
			'context':{'default_current_ids':context.get('result3')},
		}

	def open_url(self, cr, uid, ids, context=None):
		"""
			This Button method is used to open a URL
			according fields values.
		@param self: The object pointer
		@param cr: the current row, from the database cursor,
		@param uid: the current user’s ID for security checks,
		@param ids: List of IDs
		@param context: A standard dictionary for contextual values
		@return: Dictionary of values.
		"""
		for property_brw in self.browse(cr, uid, ids, context=context):
			if property_brw.street:
				address_path = (property_brw.street and (property_brw.street + ',') or ' ') + (property_brw.street2 and (property_brw.street2 + ',') or ' ') + str(property_brw.city and (property_brw.city + ',') or ' ') + str(property_brw.state_id.name and (property_brw.state_id.name + ',') or ' ') + str(property_brw.country_id.name and (property_brw.country_id.name + ',') or ' ')
				rep_address = address_path.replace(' ', '+')
				URL = "http://maps.google.com/?q=%s&ie=UTF8&z=18" % (rep_address)
				return {
					'name':'Go to website',
					'res_model':'ir.actions.act_url',
					'type':'ir.actions.act_url',
					'target':'current',
					'url': URL
					}
			else:
				raise osv.except_osv(('No Address!'), ('No Address created for this Property!'))
		return True

	def button_normal(self, cr, uid, ids, context=None):
		"""
			This Button method is used to change property state to On Lease.
		@param self: The object pointer
		@param cr: the current row, from the database cursor,
		@param uid: the current user’s ID for security checks,
		@param ids: List of IDs
		@param context: A standard dictionary for contextual values
		@return: Dictionary of values.
		"""
		return self.write(cr, uid, ids, {'state' : 'normal'}, context=context)

	def button_sold(self, cr, uid, ids, context=None):
		"""
			This Button method is used to change property state to Sold.
		@param self: The object pointer
		@param cr: the current row, from the database cursor,
		@param uid: the current user’s ID for security checks,
		@param ids: List of IDs
		@param context: A standard dictionary for contextual values
		@return: Dictionary of values.
		"""
		return self.write(cr, uid, ids, {'state' : 'sold'}, context=context)

	def button_close(self, cr, uid, ids, context=None):
		"""
			This Button method is used to change property state to Sale.
		@param self: The object pointer
		@param cr: the current row, from the database cursor,
		@param uid: the current user’s ID for security checks,
		@param ids: List of IDs
		@param context: A standard dictionary for contextual values
		@return: Dictionary of values.
		"""
		return self.write(cr, uid, ids, {'state' : 'close'}, context=context)

	def button_cancel(self, cr, uid, ids, context=None):
		"""
			This Button method is used to change property state to Cancel.
		@param self: The object pointer
		@param cr: the current row, from the database cursor,
		@param uid: the current user’s ID for security checks,
		@param ids: List of IDs
		@param context: A standard dictionary for contextual values
		@return: Dictionary of values.
		"""
		return self.write(cr, uid, ids, {'state' : 'cancel'}, context=context)

	def button_draft(self, cr, uid, ids, context=None):
		"""
			This Button method is used to change property state to Available.
		@param self: The object pointer
		@param cr: the current row, from the database cursor,
		@param uid: the current user’s ID for security checks,
		@param ids: List of IDs
		@param context: A standard dictionary for contextual values
		@return: Dictionary of values.
		"""
		return self.write(cr, uid, ids, {'state' : 'draft'}, context=context)

	def create_purchase_installment(self, cr, uid, ids, context=None):
		"""
			This Button method is used to create purchase installment
			information entries.
		@param self: The object pointer
		@param cr: the current row, from the database cursor,
		@param uid: the current user’s ID for security checks,
		@param ids: List of IDs
		@param context: A standard dictionary for contextual values
		"""
		year_create = []
		res = self.browse(cr, uid, ids[0], context=context)
		amount = res['purchase_price']
		if res['purchase_price'] == 0.0:
			raise Warning(_('Please Enter Valid Purchase Price'))
		starting_date_date = datetime.strptime(res['purchase_date'], DEFAULT_SERVER_DATE_FORMAT)
		starting_day = datetime.strptime(res['purchase_date'],DEFAULT_SERVER_DATE_FORMAT).day
		if not res['end_date']:
			raise Warning(_('Please Select End Date'))
		ending_date_date = datetime.strptime(res['end_date'], DEFAULT_SERVER_DATE_FORMAT)
		ending_day = datetime.strptime(res['end_date'], DEFAULT_SERVER_DATE_FORMAT).day
		if ending_date_date.date() < starting_date_date.date():
			raise Warning(_("Please Select End Date greater than purchase date"))
#		method used to calculate difference in month between two dates
		def diff_month(d1, d2):
			return (d1.year - d2.year)*12 + d1.month - d2.month
		difference_month = diff_month(ending_date_date, starting_date_date)
		if difference_month == 0 :
			amnt = amount
		if difference_month != 0 :
			amnt = amount/difference_month
		cr.execute("SELECT date FROM cost_cost WHERE purchase_property_id=%s" % ids[0])
		exist_dates = cr.fetchall()
		date_add = self.date_addition(res['purchase_date'], res['end_date'], res['recurring_rule_type'])
		exist_dates = map(lambda x:x[0], exist_dates)
		result = list(set(date_add) - set(exist_dates))
		result.sort(key=lambda item:item, reverse=False)
		ramnt = amnt
		remain_amnt = 0.0
		for dates in result:
			remain_amnt = amount - ramnt
			remain_amnt_per = (remain_amnt/res['purchase_price'])*100
			if remain_amnt < 0 :
				remain_amnt = remain_amnt * -1
			if remain_amnt_per < 0:
				remain_amnt_per = remain_amnt_per * -1
			year_create.append((0, 0, {
					'currency_id': res.currency_id and res.currency_id.id,
					'date':dates,
					'purchase_property_id':ids,
					'amount':amnt,
					'remaining_amount':remain_amnt,
					'rmn_amnt_per':remain_amnt_per,
					}))
			amount = remain_amnt
		return self.write(cr, uid, ids,{'purchase_cost_ids':year_create,'pur_instl_chck':True}, context=context)

	def date_addition(self, starting_date, end_date, period):
		date_list = []
		if period == 'monthly':
			while starting_date < end_date:
				if (datetime.strptime(starting_date, DEFAULT_SERVER_DATE_FORMAT)  + relativedelta(months=1)).strftime(DEFAULT_SERVER_DATE_FORMAT) > end_date:
					return date_list
				date_list.append(starting_date)
				res = ((datetime.strptime(starting_date, DEFAULT_SERVER_DATE_FORMAT) + relativedelta(months=1)).strftime(DEFAULT_SERVER_DATE_FORMAT))
				starting_date = res
			return date_list
		else:
			while starting_date < end_date:
				date_list.append(starting_date)
				res = ((datetime.strptime(starting_date, DEFAULT_SERVER_DATE_FORMAT) + relativedelta(years=1)).strftime(DEFAULT_SERVER_DATE_FORMAT))
				starting_date = res
			return date_list

	def generate_payment_entries(self, cr, uid, id, context=None):
		"""
			This Button method is used to generate property sale payment entries.
		@param self: The object pointer
		@param cr: the current row, from the database cursor,
		@param uid: the current user’s ID for security checks,
		@param ids: List of IDs
		@param context: A standard dictionary for contextual values
		"""
		data = self.browse(cr, uid, id[0], context=context)
		payment_term = data.payment_term.id
		value = data.sale_price
		amount = data.sale_price
		year_create = []
		date_ref = data.sale_date
		pterm_list = self.pool.get('account.payment.term').compute(cr, uid, payment_term, value, date_ref)
		if amount == 0.0:
			raise Warning(_('Please Enter Valid Sale Price'))
		rmnt = 0.0
		for line in pterm_list:
			lst = list(line)
			remain_amnt = amount-lst[1]
			remain_amnt_per = (remain_amnt/value)*100
			if remain_amnt < 0 :
				remain_amnt = remain_amnt * -1
			if remain_amnt_per < 0:
				remain_amnt_per = remain_amnt_per * -1
			year_create.append((0, 0, {
					'currency_id': data.currency_id and data.currency_id.id,
					'date':lst[0],
					'sale_property_id':id,
					'amount':lst[1],
					'remaining_amount':remain_amnt,
					'rmn_amnt_per':remain_amnt_per,
					}))
			amount = amount - lst[1]
		return self.write(cr, uid, id,{'sale_cost_ids':year_create,'sale_instl_chck':True})


class asset_depreciation_confirmation_wizard(osv.osv_memory):
	_inherit = "asset.depreciation.confirmation.wizard"
	_description = "asset.depreciation.confirmation.wizard"


	def asset_compute(self, cr, uid, ids, context):
		"""
			This method is used to compute asset from depreciation wizard.
		@param self: The object pointer
		@param cr: the current row, from the database cursor,
		@param uid: the current user’s ID for security checks,
		@param ids: List of IDs
		@param context: A standard dictionary for contextual values
		@return: Dictionary of values.
		"""
		ass_obj = self.pool.get('account.asset.asset')
		rec = ids and ids[0]
		asset_ids = ass_obj.search(cr, uid, [('state', 'in', ('draft', 'normal', 'close', 'sold', 'book'))], context=context)
		data = self.browse(cr, uid, rec, context=context)
		period_id = data.period_id and data.period_id.id or False
		created_move_ids = ass_obj._compute_entries(cr, uid, asset_ids, period_id, context=context)
		return {
			'name': _('Created Asset Moves'),
			'view_type': 'form',
			'view_mode': 'tree,form',
			'res_model': 'account.move',
			'view_id': False,
			'domain': "[('id','in',[" + ','.join(map(str, created_move_ids)) + "])]",
			'type': 'ir.actions.act_window',
		}


class account_asset_depreciation_line(osv.osv):
	_inherit = 'account.asset.depreciation.line'
	
	def open_account_move(self, cr, uid, ids, context=None):
		"""
		@param self: The object pointer
		@param cr: the current row, from the database cursor,
		@param uid: the current user’s ID for security checks,
		@param ids: List of IDs
		@param context: A standard dictionary for contextual values
		"""
		open_move_id = self.pool.get('ir.model.data').get_object_reference(cr, uid, 'account', 'view_move_form')[1]
		open_move_obj = self.browse(cr, uid, ids, context=context)
		return {
			'view_type': 'form',
			'view_id': open_move_id,
			'view_mode': 'form',
			'res_model': 'account.move',
			'res_id':open_move_obj.move_id.id,
			'type': 'ir.actions.act_window',
			'target': 'current',
			'context': context,
			}
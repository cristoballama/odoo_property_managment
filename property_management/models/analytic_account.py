# -*- coding: utf-8 -*-
##############################################################################
#
#	OpenERP, Open Source Management Solution
#	Copyright (C) 2011-Today Serpent Consulting Services PVT LTD (<http://www.serpentcs.com>)
#
#	This program is free software: you can redistribute it and/or modify
#	it under the terms of the GNU Affero General Public License as
#	published by the Free Software Foundation, either version 3 of the
#	License, or (at your option) any later version.
#
#	This program is distributed in the hope that it will be useful,
#	but WITHOUT ANY WARRANTY; without even the implied warranty of
#	MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
#	GNU Affero General Public License for more details.
#
#	You should have received a copy of the GNU Affero General Public License
#	along with this program.  If not, see <http://www.gnu.org/licenses/>.
#
############################################################################
from datetime import datetime, timedelta
from dateutil.relativedelta import relativedelta
from openerp.osv import osv, fields
from openerp.tools.translate import _
from openerp.exceptions import Warning
from openerp.tools import DEFAULT_SERVER_DATE_FORMAT


class account_analytic_account(osv.osv):
	_inherit = "account.analytic.account"

	def _total_deb_cre_amt(self, cr, uid, ids, name, arg, context=None):
		"""
			This method is used to calculate Total income amount.
		@param self: The object pointer
		@param cr: the current row, from the database cursor,
		@param uid: the current user’s ID for security checks,
		@param ids: List of IDs
		@param name: Names of fields.
		@param arg: User defined arguments
		@param context: A standard dictionary for contextual values
		@return: Dictionary of values.
		"""
		res = {}
		total = 0.0
		for tenancy_brw in self.browse(cr, uid, ids, context=context):
			total = tenancy_brw.total_debit_amt - tenancy_brw.total_credit_amt
			res[tenancy_brw.id] = total
		return res

	def _total_credit_amt(self, cr, uid, ids, name, arg, context=None):
		"""
			This method is used to calculate Total credit amount.
		@param self: The object pointer
		@param cr: the current row, from the database cursor,
		@param uid: the current user’s ID for security checks,
		@param ids: List of IDs
		@param name: Names of fields.
		@param arg: User defined arguments
		@param context: A standard dictionary for contextual values
		@return: Dictionary of values.
		"""
		res = {}
		total = 0.0
		for tenancy_brw in self.browse(cr, uid, ids, context=context):
			if tenancy_brw.account_move_line_ids and tenancy_brw.account_move_line_ids.ids:
				for debit_amt in tenancy_brw.account_move_line_ids:
					total += debit_amt.credit
			if tenancy_brw.account_move_line_deposit_ids and tenancy_brw.account_move_line_deposit_ids.ids:
				for credit_amt in tenancy_brw.account_move_line_deposit_ids:
					total += credit_amt.credit
			res[tenancy_brw.id] = total
		return res

	def _total_debit_amt(self, cr, uid, ids, name, arg, context=None):
		"""
			This method is used to calculate Total debit amount.
		@param self: The object pointer
		@param cr: the current row, from the database cursor,
		@param uid: the current user’s ID for security checks,
		@param ids: List of IDs
		@param name: Names of fields.
		@param arg: User defined arguments
		@param context: A standard dictionary for contextual values
		@return: Dictionary of values.
		"""
		res = {}
		total = 0.0
		for tenancy_brw in self.browse(cr, uid, ids, context=context):
			if tenancy_brw.account_move_line_ids and tenancy_brw.account_move_line_ids.ids:
				for debit_amt in tenancy_brw.account_move_line_ids:
					total += debit_amt.debit
			if tenancy_brw.account_move_line_deposit_ids and tenancy_brw.account_move_line_deposit_ids.ids:
				for credit_amt in tenancy_brw.account_move_line_deposit_ids:
					total += credit_amt.debit
			res[tenancy_brw.id] = total
		return res

	def _total_amount_rent(self, cr, uid, ids, name, arg, context=None):
		"""
			This method is used to calculate Total Rent of current Tenancy.
		@param self: The object pointer
		@param cr: the current row, from the database cursor,
		@param uid: the current user’s ID for security checks,
		@param ids: List of IDs
		@param name: Names of fields.
		@param arg: User defined arguments
		@param context: A standard dictionary for contextual values
		@return: Dictionary of values.
		"""
		res = {}
		tot = 0.00
		for tenancy_brw in self.browse(cr, uid, ids, context=context):
			for rent_brw in tenancy_brw.rent_schedule_ids:
				tot += rent_brw.amount
			res[tenancy_brw.id] = tot
		return res

	def _total_amount_deposit(self, cr, uid, ids, name, arg, context=None):
		"""
			This method is used to used to calculate Deposit for current Tenancy.
		@param self: The object pointer
		@param cr: the current row, from the database cursor,
		@param uid: the current user’s ID for security checks,
		@param ids: List of IDs
		@param name: Names of fields.
		@param arg: User defined arguments
		@param context: A standard dictionary for contextual values
		@return: Dictionary of values.
		"""
		res = {}
		for tenancy_brw in self.browse(cr, uid, ids, context=context):
			res[tenancy_brw.id] = tenancy_brw.amount_fee_paid
		return res

	def _get_deposit(self, cr, uid, ids, name, arg, context=None):
		"""
			This method is used to set deposit return and deposit received
			boolean field accordingly to current Tenancy.
		@param self: The object pointer
		@param cr: the current row, from the database cursor,
		@param uid: the current user’s ID for security checks,
		@param ids: List of IDs
		@param name: Names of fields.
		@param arg: User defined arguments
		@param context: A standard dictionary for contextual values
		@return: Dictionary of values.
		"""
		res = {}
		voucher_pool = self.pool.get('account.voucher')
		for tennancy in self.browse(cr, uid, ids, context=context):
			res[tennancy.id] = {'deposit_return':False, 'deposit_received':False}
			voucher_ids = voucher_pool.search(cr, uid, [('tenancy_id', '=', tennancy.id), ('state', '=', 'posted')], context=context)
			if voucher_ids:
				for voucher in voucher_pool.browse(cr, uid, voucher_ids, context=context):
					if voucher.type == 'payment':
						res[tennancy.id]['deposit_return'] = True
					elif voucher.type == 'receipt' and tennancy.total_deposit == voucher.amount:
						res[tennancy.id]['deposit_received'] = True
		return res


	_columns = {
		'rent':fields.float('Rent'),
		'deposit':fields.float('Deposit'),
		'property_id':fields.many2one('account.asset.asset','Property'),
		'account_move_line_ids': fields.one2many('account.move.line', 'analytic_account_id', 'Entries', readonly=True, states={'draft':[('readonly',False)]}),
		'account_move_line_deposit_ids': fields.one2many('account.move.line', 'new_tenancy_id', 'Deposite History', readonly=True, states={'draft':[('readonly',False)]}),
		'tenant_id':fields.many2one('tenant.partner', 'Tenant', domain="[('tenant', '=', True)]"),
		'early_warning':fields.boolean('Early Warning'),
		'ew_weeks':fields.integer('EW Weeks'),
		'total_rent':fields.function(_total_amount_rent, method=True, type='float', string='Total Rent', readonly=True,store=True),
		'total_deposit':fields.function(_total_amount_deposit, method=True, type='float', string='Total Deposit', readonly=True,store=True),
		'deposit_scheme_type':fields.selection([('insurance', 'Insurance-based'),], 'Type Of Scheme'),
		'contact_id':fields.many2one('res.partner', 'Contact',),
		'amount_fee_paid':fields.integer('Amount Of Fee Paid'),
		'duration_cover':fields.text('Duration Of Cover'),
		'amount_return':fields.integer('Amount Returned'),
		'utility_ids':fields.one2many('property.utility', 'tenancy_id', 'Utilities'),
		'rent_schedule_ids':fields.one2many('tenancy.rent.schedule', 'tenancy_id', 'Rent Schedule'),
		'contract_attachment' : fields.binary('Tenancy Contract'),
		'deposit_received': fields.function(_get_deposit, method=True, multi='deposit', type='boolean', string='Deposit Received?'),
		'deposit_return':fields.function(_get_deposit, method=True, multi='deposit', type='boolean', string='Deposit Returned?'),
		'doc_name' : fields.char('Filename'),
		'note':fields.text('Notes'),
		'is_property':fields.boolean('Is Property?'),
		'rent_entry_chck':fields.boolean('Rent Entries Check'),
		'total_debit_amt':fields.function(_total_debit_amt, method=True, type='float', string='Total Debit Amount'),
		'total_credit_amt':fields.function(_total_credit_amt, method=True, type='float', string='Total Credit Amount'),
		'total_deb_cre_amt':fields.function(_total_deb_cre_amt, method=True, type='float', string='Total Expenditure'),
	}

	_defaults = {
		'state': 'draft',
		'rent_entry_chck':False,
	}

	def create(self, cr, uid, vals, context=None):
		"""
			This Method is used to overrides orm create method.
		@param self: The object pointer
		@param cr: the current row, from the database cursor,
		@param uid: the current user’s ID for security checks,
		@param vals: dictionary of fields value.
		@param context: A standard dictionary for contextual values
		"""
		if vals.has_key('tenant_id'):
			vals.update({'is_property':True})
		if vals.has_key('property_id'):
			self.pool.get('account.asset.asset').write(cr, uid, vals['property_id'],
							{'current_tenant': vals['tenant_id'],'state': 'book'}, context=context)
		return super(account_analytic_account, self).create(cr, uid, vals, context=context)

	# while edit name of tenant, it will write in invisible field current tenant.
	def write(self, cr, uid, ids, vals, context=None):
		"""
			This Method is used to overrides orm write method.
		@param self: The object pointer
		@param cr: the current row, from the database cursor,
		@param uid: the current user’s ID for security checks,
		@param ids: List of IDs
		@param vals: dictionary of fields value.
		@param context: A standard dictionary for contextual values
		"""
		asset_obj = self.pool.get('account.asset.asset')
		for tenancy_rec in self.browse(cr, uid, ids, context=context):
			rec = super(account_analytic_account, self).write(cr, uid, ids, vals, context=context)
			if vals.get('state'):
				if vals['state'] == 'open':
					current_tenant = asset_obj.write(cr, uid, [tenancy_rec.property_id.id],
													{'current_tenant':tenancy_rec.tenant_id.id,
													'state': 'normal'},
													context=context)
				if vals['state'] == 'close':
					current_tenant = asset_obj.write(cr, uid, [tenancy_rec.property_id.id],
													{'state': 'draft','current_tenant':False},
													context=context)
			return rec

	def unlink(self, cr, uid, ids, context=None):
		"""
			Overrides orm unlink method.
		@param self: The object pointer
		@param cr: the current row, from the database cursor,
		@param uid: the current user’s ID for security checks,
		@param ids: List of IDs
		@param context: A standard dictionary
		@return: True/False.
		"""
		rent_ids = []
		asset_obj = self.pool.get('account.asset.asset')
		analytic_line_obj = self.pool.get('account.analytic.line')
		for tenancy_rec in self.browse(cr, uid, ids, context=context):
			current_tenant = asset_obj.write(cr, uid, [tenancy_rec.property_id.id],
											 {'state': 'draft','current_tenant':False},
											 context=context)
			analytic_ids = analytic_line_obj.search(cr,uid,[('account_id','=',tenancy_rec.id)],context=context)
			if len(analytic_ids) != 0:
				analytic_line_obj.unlink(cr,uid, analytic_ids,context=context)
			if tenancy_rec.rent_schedule_ids and tenancy_rec.rent_schedule_ids.ids:
				for rent_rec in tenancy_rec.rent_schedule_ids:
					if rent_rec.move_check == True:
						raise Warning(_('You cannot delete Tenancy record, if any related Rent Schedule entries are in posted.'))
					rent_ids.append(rent_rec.id)
				self.pool.get('tenancy.rent.schedule').unlink(cr, uid , rent_ids, context=context)

			if tenancy_rec.property_id.property_manager and tenancy_rec.property_id.property_manager.id:
				releted_user = tenancy_rec.property_id.property_manager.id
				new_id = self.pool.get('res.users').search(cr,uid,[('partner_id','=',releted_user)],context=context)
				if releted_user and new_id:
					self.pool.get('res.users').write(cr, uid, new_id, {'tenant_ids':[(3, tenancy_rec.tenant_id.id)]} ,context=context)

		return super(account_analytic_account, self).unlink(cr, uid , ids, context=context)

	def onchange_property_id(self, cr, uid, ids, property_id, context=None):
		"""
			This Method is used to set property related fields value.
		@param self: The object pointer
		@param cr: the current row, from the database cursor,
		@param uid: the current user’s ID for security checks,
		@param ids: List of IDs
		@param context: A standard dictionary for contextual values
		@return: Dictionary of values.
		"""
		property_data = self.pool.get('account.asset.asset').browse(cr, uid, property_id, context=context)
		return { 'value' : {'rent' : property_data.ground_rent or 0.0}}

	def onchange_deposite(self, cr, uid, ids, deposit, context=None):
		"""
			When you change Deposit field value, this method will change
			amount_fee_paid field value accordingly.
		@param self: The object pointer
		@param cr: the current row, from the database cursor,
		@param uid: the current user’s ID for security checks,
		@param ids: List of IDs
		@param context: A standard dictionary for contextual values
		@return: Dictionary of values.
		"""
		return { 'value' : {'amount_fee_paid':int(deposit)}}


	def button_receive(self, cr, uid, ids, context=None):
		"""
			This button method is used to open the related
			account voucher form view.
		@param self: The object pointer
		@param cr: the current row, from the database cursor,
		@param uid: the current user’s ID for security checks,
		@param ids: List of IDs
		@param context: A standard dictionary for contextual values
		"""
		if not ids: return []
		line_data = []
		obj_ir_model_data = self.pool.get('ir.model.data')
		tenancy_rec = self.browse(cr, uid, ids[0], context=context)
		if tenancy_rec.deposit == 0.00:
			raise Warning(_('Please Enter Deposit amount'))
		if not tenancy_rec.property_id.income_acc.id:
			raise Warning(_('Please Configure Income Account from Property'))
		ir_id = obj_ir_model_data._get_id(cr, uid, 'account_voucher', 'view_voucher_form')
		ir_rec = obj_ir_model_data.browse(cr, uid, ir_id, context=context)
		line_data.append({'name' : 'deposit', 'account_id' : tenancy_rec.tenant_id.parent_id.property_account_receivable.id, 'type' : 'cr', 'property_id' : tenancy_rec.property_id.id, 'amount' : tenancy_rec.deposit})
		return {
			'view_mode': 'form',
			'view_id': [ir_rec.res_id],
			'view_type': 'form',
			'res_model': 'account.voucher',
			'type': 'ir.actions.act_window',
			'nodestroy': True,
			'target': 'current',
			'domain': '[]',
			'context': {
				'default_partner_id': tenancy_rec.tenant_id.parent_id.id,
				'default_journal_id' : False,
				'default_name' : 'Deposit Received',
				'default_type' : 'receipt',
				'default_reference':tenancy_rec.code,
				'default_line_ids' : line_data,
				'default_property_id' : tenancy_rec.property_id and tenancy_rec.property_id.id,
				'default_tenancy_id' : tenancy_rec.id,
				'close_after_process': True,
			}
		}

	def button_return(self, cr, uid, ids, context=None):
		"""
			This button method is used to open the related account voucher
			form view.
		@param self: The object pointer
		@param cr: the current row, from the database cursor,
		@param uid: the current user’s ID for security checks,
		@param ids: List of IDs
		@param context: A standard dictionary for contextual values
		"""
		if not ids: return []
		line_data = []
		obj_ir_model_data = self.pool.get('ir.model.data')
		tenancy_rec = self.browse(cr, uid, ids[0], context=context)
		if tenancy_rec.amount_return == 0:
			raise Warning(_('Please Enter Deposit Returned amount'))
		if not tenancy_rec.property_id.income_acc.id:
			raise Warning(_('Please Configure Income Account from Property'))
		ir_id = obj_ir_model_data._get_id(cr, uid, 'account_voucher', 'view_voucher_form')
		ir_rec = obj_ir_model_data.browse(cr, uid, ir_id, context=context)
		line_data.append({'name' : 'deposit', 'property_id' : tenancy_rec.property_id.id, 'account_id' : tenancy_rec.tenant_id.parent_id.property_account_receivable.id, 'type' : 'dr', 'amount' : tenancy_rec.amount_return})
		return {
			'view_mode': 'form',
			'view_id': [ir_rec.res_id],
			'view_type': 'form',
			'res_model': 'account.voucher',
			'type': 'ir.actions.act_window',
			'nodestroy': True,
			'target': 'current',
			'domain': '[]',
			'context': {
				'default_partner_id': tenancy_rec.tenant_id.parent_id.id,
				'default_journal_id' : False,
				'default_name' : 'Deposit Return',
				'default_type' : 'payment',
				'default_reference':tenancy_rec.code,
				'default_line_ids' : line_data,
				'default_property_id' : tenancy_rec.property_id and tenancy_rec.property_id.id,
				'default_tenancy_id' : tenancy_rec.id,
				'close_after_process': True
			}
		}


	def button_start(self, cr, uid, ids, context=None):
		"""
			This button method is used to Change Tenancy state to Open.
		@param self: The object pointer
		@param cr: the current row, from the database cursor,
		@param uid: the current user’s ID for security checks,
		@param ids: List of IDs
		@param context: A standard dictionary for contextual values
		"""
		current_rec = self.browse(cr, uid , ids[0], context=context)
		if current_rec.property_id.property_manager and current_rec.property_id.property_manager.id:
			releted_user = current_rec.property_id.property_manager.id
			new_id = self.pool.get('res.users').search(cr,uid,[('partner_id','=',releted_user)],context=context)
			if releted_user and new_id:
				self.pool.get('res.users').write(cr, uid, new_id, {'tenant_ids':[(4, current_rec.tenant_id.id)]} ,context=context)
		return self.write(cr, uid, ids, {'state':'open'}, context=context)

	def button_close(self, cr, uid, ids, context=None):
		"""
			This button method is used to Change Tenancy state to close.
		@param self: The object pointer
		@param cr: the current row, from the database cursor,
		@param uid: the current user’s ID for security checks,
		@param ids: List of IDs
		@param context: A standard dictionary for contextual values
		"""
		return self.write(cr, uid, ids, {'state':'close'}, context=context)

	def button_set_to_draft(self, cr, uid, ids, context=None):
		"""
			This Method is used to open tenancy renew wizard.
		@param self: The object pointer
		@param cr: the current row, from the database cursor,
		@param uid: the current user’s ID for security checks,
		@param ids: List of IDs
		@param context: A standard dictionary for contextual values
		@return: Dictionary of values.
		"""
		if context is None:
			context = {}
		tenancy_rent_ids = self.pool.get('tenancy.rent.schedule').search(cr, uid, [('tenancy_id', '=', ids[0]), ('move_check', '=',False)], context=context)
		if len(tenancy_rent_ids) > 0:
			raise Warning(_('In order to Renew a Tenancy, Please make all related Rent Schedule entries in posted.'))
		tenancy_brw = self.browse(cr, uid, ids[0], context=context)
		context.update({'edate': tenancy_brw.date})
		return {
			'name': ('Tenancy Renew Wizard'),
			'res_model': 'renew.tenancy',
			'type': 'ir.actions.act_window',
			'view_id': False,
			'view_mode': 'form',
			'view_type': 'form',
			'target': 'new',
			'context': {'default_start_date': context.get('edate')}
		}

	def cron_property_states_changed(self, cr, uid, context=None):
		"""
			This Method is called by Scheduler for change property state
			according to tenancy state per day.
		@param self: The object pointer
		@param cr: the current row, from the database cursor,
		@param uid: the current user’s ID for security checks,
		@param context: A standard dictionary for contextual values
		"""
		date = datetime.now().strftime(DEFAULT_SERVER_DATE_FORMAT)
		date_now = datetime.strptime( date, DEFAULT_SERVER_DATE_FORMAT)
		close_tncy_ids = self.search(cr, uid, [('state','=','close'),('is_property','=',True)], context=context)
		tncy_ids = self.search(cr, uid, [('date_start','<=',date_now),('date','>=',date_now),('state','=','open'),('is_property','=',True)], context=context)
		if len(tncy_ids) != 0:
			for tncy_data in self.browse(cr, uid, tncy_ids, context = context):
				property_id = tncy_data.property_id and tncy_data.property_id.id
				if property_id:
					self.pool.get('account.asset.asset').write(cr, uid, [property_id], {'state':'normal','color':7}, context=context)
		if len(close_tncy_ids) != 0:
			for tncy_data in self.browse(cr, uid, close_tncy_ids, context = context):
				property_id = tncy_data.property_id and tncy_data.property_id.id
				if property_id:
					self.pool.get('account.asset.asset').write(cr, uid, [property_id], {'state':'draft','color':4}, context=context)
		return True 

	def cron_property_tenancy(self, cr, uid, context=None):
		"""
			This Method is called by Scheduler to send email
			to tenant as a reminder for rent payment.
		@param self: The object pointer
		@param cr: the current row, from the database cursor,
		@param uid: the current user’s ID for security checks,
		@param context: A standard dictionary for contextual values
		"""
		tenancy_ids = []
		due_date = datetime.now().date() + relativedelta(days=7)
		tncy_ids = self.search(cr, uid, [('is_property','=',True),('state','=','open')], context=context)
		for tncy_data in self.browse(cr, uid ,tncy_ids, context=context):
			tncy_rent_ids = self.pool.get('tenancy.rent.schedule').search(cr, uid, [('tenancy_id','=',tncy_data.id),
																	  ('start_date','=',due_date)], context=context)
			if tncy_rent_ids:
				tenancy_ids.append(tncy_data.id)
		tenancy_sort_ids = list(set(tenancy_ids))
		model_data_id = self.pool.get('ir.model.data').get_object_reference(cr, uid, 'property_management', 'property_email_template')[1]
		for tenancy in tenancy_sort_ids:
			self.pool.get('email.template').send_mail(cr, uid, model_data_id, tenancy, force_send=True, context=context)
		return True

	def create_rent_schedule(self, cr, uid, ids, context=None):
		"""
			This button method is used to create rent schedule Lines.
		@param self: The object pointer
		@param cr: the current row, from the database cursor,
		@param uid: the current user’s ID for security checks,
		@param ids: List of IDs
		@param context: A standard dictionary for contextual values
		"""
		year_create = []
		rec_id = ids and ids[0]
		res = self.browse(cr, uid, rec_id, context=context)
		if not res.property_id.income_acc.id:
			raise Warning(_('Please Configure Income Account from Property'))
		cr.execute("SELECT start_date FROM tenancy_rent_schedule WHERE tenancy_id=%s" % ids[0])
		exist_dates = cr.fetchall()
		date_add = self.date_addition(res['date_start'], res['date'], res['recurring_rule_type'])
		exist_dates = map(lambda x:x[0], exist_dates)
		result = list(set(date_add) - set(exist_dates))
		for dates in result:
			year_create.append((0, 0, {
				'start_date':dates,
				'tenancy_id':rec_id,
				'amount':res['rent'],
				'property_id':res.property_id and res.property_id.id or False
			}))
		return self.write(cr, uid, [rec_id], {'rent_schedule_ids':year_create,'rent_entry_chck':True})

	def date_addition(self, starting_date, end_date, period):
		date_list = []
		if period == 'monthly':
			while starting_date <= end_date:
				date_list.append(starting_date)
				res = ((datetime.strptime(starting_date, DEFAULT_SERVER_DATE_FORMAT) + relativedelta(months=1)).strftime(DEFAULT_SERVER_DATE_FORMAT))
				starting_date = res
			return date_list
		elif period == 'daily':
			while starting_date <= end_date:
				date_list.append(starting_date)
				res = ((datetime.strptime(starting_date, DEFAULT_SERVER_DATE_FORMAT) + relativedelta(days=1)).strftime(DEFAULT_SERVER_DATE_FORMAT))
				starting_date = res
			return date_list
		elif period == 'weekly':
			while starting_date <= end_date:
				date_list.append(starting_date)
				res = ((datetime.strptime(starting_date, DEFAULT_SERVER_DATE_FORMAT) + relativedelta(weeks=1)).strftime(DEFAULT_SERVER_DATE_FORMAT))
				starting_date = res
			return date_list
		else:
			while starting_date <= end_date:
				date_list.append(starting_date)
				res = ((datetime.strptime(starting_date, DEFAULT_SERVER_DATE_FORMAT) + relativedelta(years=1)).strftime(DEFAULT_SERVER_DATE_FORMAT))
				starting_date = res
			return date_list

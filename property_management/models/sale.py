# -*- coding: utf-8 -*-
##############################################################################
#
#	OpenERP, Open Source Management Solution
#	Copyright (C) 2011-Today Serpent Consulting Services PVT LTD (<http://www.serpentcs.com>)
#
#	This program is free software: you can redistribute it and/or modify
#	it under the terms of the GNU Affero General Public License as
#	published by the Free Software Foundation, either version 3 of the
#	License, or (at your option) any later version.
#
#	This program is distributed in the hope that it will be useful,
#	but WITHOUT ANY WARRANTY; without even the implied warranty of
#	MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
#	GNU Affero General Public License for more details.
#
#	You should have received a copy of the GNU Affero General Public License
#	along with this program.  If not, see <http://www.gnu.org/licenses/>.
#
############################################################################

from openerp.osv import osv, fields


class sale_order_line(osv.osv):
	_inherit = "sale.order.line"

	_columns = {
		'property_id': fields.many2one('account.asset.asset', 'Property'),
		'is_property':fields.boolean('Is Property'),
		'price_unit':fields.float('Price Unit'),
		'annual_rent':fields.float('Annual Rent'),
		}

	def sale_property_onchange(self, cr, uid, ids, property_id,rent_bool,context=None):
		"""
			when you change property field, this method will change
			property price accordingly.
		@param self: The object pointer
		@param cr: the current row, from the database cursor,
		@param uid: the current user’s ID for security checks,
		@param ids: List of IDs
		@param context: A standard dictionary for contextual values
		@return: Dictionary of values.
		"""
		val = {}
		sale_price = 0.0
		if property_id:
			parent_data = self.pool.get('account.asset.asset').browse(cr, uid, property_id, context=context)
			if rent_bool == False:
				sale_price = parent_data.sale_price
			val.update({
				'product_uom_qty':1,
				'name':parent_data.name or False,
				'price_unit':sale_price,
				})
		return { 'value' : val}

	def sale_rent_property_onchange(self, cr, uid, ids, property_id,rent_bool,context=None):
		"""
			when you change property field, this method will change
			property price accordingly.
		@param self: The object pointer
		@param cr: the current row, from the database cursor,
		@param uid: the current user’s ID for security checks,
		@param ids: List of IDs
		@param context: A standard dictionary for contextual values
		@return: Dictionary of values.
		"""
		val = {}
		if property_id:
			price_unit = 0.0
			annual_price = 0.0
			parent_data = self.pool.get('account.asset.asset').browse(cr, uid, property_id, context=context)
			if rent_bool == True and parent_data.ground_rent:
					price_unit = parent_data.ground_rent
			val.update({'product_uom_qty':1,
						'name':parent_data.name or False,
						'price_unit':price_unit,
						})
		return { 'value' : val}


class sale_order(osv.osv):
	_inherit = "sale.order"

	_columns = {
			'is_property':fields.boolean('Is Property'),
			'rent_bool':fields.boolean('Property On Rent ?',help='checked if make quotation for Rented property'),
			'order_line2': fields.one2many('sale.order.line', 'order_id', 'Order Lines'),
			}

	_defaults = {
				'is_property': False,
				 }

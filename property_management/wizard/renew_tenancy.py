# -*- encoding: utf-8 -*-
##############################################################################
#
#	OpenERP, Open Source Management Solution
#	Copyright (C) 2012-Today Serpent Consulting Services Pvt. Ltd. (<http://www.serpentcs.com>)
#
#	This program is free software: you can redistribute it and/or modify
#	it under the terms of the GNU General Public License as published by
#	the Free Software Foundation, either version 3 of the License, or
#	(at your option) any later version.
#
#	This program is distributed in the hope that it will be useful,
#	but WITHOUT ANY WARRANTY; without even the implied warranty of
#	MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
#	GNU General Public License for more details.
#
#	You should have received a copy of the GNU General Public License
#	along with this program.  If not, see <http://www.gnu.org/licenses/>
#
##############################################################################

from openerp.osv import orm, fields

class wizard_renew_tenancy(orm.TransientModel):
	_name = 'renew.tenancy'

	_columns = {
		'start_date':fields.date('Start Date'),
		'end_date':fields.date('End Date'),
		'recurring_rule_type': fields.selection([('daily', 'Day(s)'),('weekly', 'Week(s)'),
												 ('monthly', 'Month(s)'),('yearly', 'Year(s)'),
												 ], 'Recurrency', help="Invoice automatically repeat at specified interval"),
		}

	def renew_contract(self,cr,uid,ids,context=None):
		"""
			This Method is used to Renew Tenancy.
		@param self: The object pointer
		@param cr: the current row, from the database cursor,
		@param uid: the current user’s ID for security checks,
		@param ids: List of IDs
		@param context: A standard dictionary for contextual values
		@return: Dictionary of values.
		"""
		if context is None:
			context = {}
		modid = self.pool.get('ir.model.data').get_object_reference(cr, uid, 'property_management', 'property_analytic_view_form')
		if context.get('active_ids', []):
			for rec in self.browse(cr,uid,ids,context=context):
				vals = {'date_start' : rec.start_date,
						'date' : rec.end_date,
						'recurring_rule_type' : rec.recurring_rule_type,
						'state':'draft',
						'rent_entry_chck':False,
						}
				self.pool.get('account.analytic.account').write(cr,uid,context['active_ids'],vals,context=context)
		return {
			'view_mode': 'form',
			'view_id': modid[1],
			'view_type': 'form',
			'res_model': 'account.analytic.account',
			'type': 'ir.actions.act_window',
			'target': 'current',
			'res_id': context['active_ids'][0],
			}
